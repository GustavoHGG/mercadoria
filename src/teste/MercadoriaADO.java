package teste;

public class MercadoriaADO implements MercadoriaBD{

    @Override
    public void obterMercadoria() {
        System.out.println("Consulta ADO");
    }

    @Override
    public void inserirMercadoria() {
        System.out.println("Inserção ADO");
    }
    
}
